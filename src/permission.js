import router from './router'
import store from './store'
// eslint-disable-next-line no-unused-vars
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login', '/auth-redirect'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = getToken()

  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/' })
      NProgress.done()
    } else {
      console.log("判断是否用户权限")
      // determine whether the user has obtained his permission roles through getInfo
      // console.log(store.getters.roles)
      const hasRoles = store.getters.roles && store.getters.roles.length > 0
      if (hasRoles) {
        // eslint-disable-next-line no-unused-vars
        const accessRoutes = await store.dispatch('permission/generateRoutes', [])
        console.log("权限路由")
        // console.log(accessRoutes);
        next()
      } else {
        try {
         
          console.log("添加路由");
          // get user info
          // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
          const { roles } = store.getters.roles
          // console.log(roles);
          // generate accessible routes map based on roles
          const accessRoutes = await store.dispatch('permission/generateRoutes', roles)
          // console.log(accessRoutes);
          // dynamically add accessible routes
          router.addRoutes(accessRoutes)
          // console.log(router.options.routes);
          // hack method to ensure that addRoutes is complete
          // set the replace: true, so the navigation will not leave a history record
          next({ ...to, replace: false })
        } catch (error) {
          console.log("error");
         
          // remove token and go to login page to re-login
          // await store.dispatch('user/resetToken')
          // Message.error(error || 'Has Error')
          // eslint-disable-next-line no-empty
          if(hasToken){
            // eslint-disable-next-line no-unused-vars
            const accessRoutes = await store.dispatch('permission/generateRoutes', [])
            console.log("看返回值")
            // console.log(accessRoutes);
            next()
             // next(`/login?redirect=${to.path}`)
          }else{
            next(`/login?redirect=${to.path}`)
          }
         
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/
console.log("没有tokend")
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      console.log("跳回登录")
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
