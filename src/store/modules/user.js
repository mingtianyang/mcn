// eslint-disable-next-line no-unused-vars
import { login, logout, getInfo ,rushtoken} from '@/api/user';
import { getToken, setToken, removeToken,setRole } from '@/utils/auth';
import router, { resetRouter } from '@/router';
// eslint-disable-next-line no-unused-vars
import Cookies from 'js-cookie'
import defined from '../../changeuser';
const state = {
  token: getToken(),
  name: '',
  avatar: '',
  introduction: '',
  roles: [],
  userId:''
};

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token;
  },
  SET_INTRODUCTION: (state, introduction) => {
    state.introduction = introduction;
  },
  SET_NAME: (state, name) => {
    state.name = name;
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar;
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles;
  },
  SET_USERID: (state, userId) => {
    state.userId = userId;
  },
};

const actions = {
  
  // user login
  login({ commit }, userInfo) {
    console.log("登录校验");
    console.log(commit);
    console.log(userInfo);
    // const { username, password } = userInfo;
    return new Promise((resolve, reject) => {
      login(userInfo).then((response) => {
        
        console.log('获取返回值11');
        console.log(response);
        const { data } = response;
        console.log(data);
        commit('SET_TOKEN', data.authToken.accessToken);
        Cookies.set('userinfo', data)
        setToken(data.authToken.accessToken);
        setRole(JSON.stringify(data.roles));
        defined.setName(data.nickName);
        defined.setavatorUrl(data.avatorUrl);
        commit('SET_ROLES', data.roles);
        commit('SET_NAME', data.nickName);
        commit('SET_AVATAR', data.avatorUrl);
        commit('SET_USERID',data.userId);
        commit('SET_INTRODUCTION', '超级管理员');
        setInterval(() => {
          console.log("刷新秘钥")
          let data=JSON.parse(Cookies.get('userinfo'));
          let sendata={
              'api_token':'api_xiangmu',
              'access_token':data.authToken.accessToken,
              'user_id':data.userId,
              'refresh_token':data.authToken.refreshToken,
            }
          
          rushtoken(sendata).then((myres)=>{
            console.log("刷新秘钥")
            // Cookies.set('userinfo', myres)
            setToken(myres.data.accessToken);
           
          })
        }, 7200000);
 
        resolve();
      }).catch((error) => {
        console.log(error);
        reject(error);
      });
    });
  },

  // get user info
  getInfo({ commit, state },userInfo) {
    // eslint-disable-next-line no-unused-vars
    const { username, password } = userInfo;
    return new Promise((resolve, reject) => {
      getInfo({ username: username.trim(), password }).then((response) => {
        const { data } = state;

        if (!response) {
          reject('Verification failed, please Login again.');
        }

        console.log('getinfor返回值11');
        console.log(state);
    
        commit('SET_TOKEN', data.token);
        setToken(data.token);
        commit('SET_ROLES', data.roles);
        commit('SET_NAME', data.username);
       
        commit('SET_AVATAR', "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        commit('SET_INTRODUCTION', '超级管理员');
        resolve(data);
      }).catch((error) => {
        reject(error);
      });
    });
  },

  // user logout
  // eslint-disable-next-line no-unused-vars
  logout({ commit, state, dispatch }) {
    // eslint-disable-next-line no-unused-vars
    return new Promise((resolve, reject) => {
      // logout(state.token).then(() => {
        commit('SET_TOKEN', '');
        commit('SET_ROLES', []);
        removeToken();
        resetRouter();
      
        // reset visited views and cached views
        // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
        dispatch('tagsView/delAllViews', null, { root: true });

        resolve();
      // }).catch((error) => {
      //   reject(error);
      // });
    });
  },

  // remove token
  resetToken({ commit }) {
    return new Promise((resolve) => {
      commit('SET_TOKEN', '');
      commit('SET_ROLES', []);
      removeToken();
      resolve();
    });
  },

  // dynamically modify permissions
  changeRoles({ commit, dispatch }, role) {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve) => {
      const token = `${role}-token`;

      commit('SET_TOKEN', token);
      setToken(token);

      const { roles } = await dispatch('getInfo');

      resetRouter();

      // generate accessible routes map based on roles
      const accessRoutes = await dispatch('permission/generateRoutes', roles, { root: true });
      console.log(accessRoutes);
      // dynamically add accessible routes
      router.addRoutes(accessRoutes);

      // reset visited views and cached views
      dispatch('tagsView/delAllViews', null, { root: true });

      resolve();
    });
  },
 
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
