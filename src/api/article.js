import request from '@/utils/request';

export function fetchList(query) {
 
  return request({
    url: '/api/v2/stock/info/',
    method: 'get',
    params: query,
  });
}
export function fetStockInfo(query) {
 const url='/api/v2/stock/info/'+query;
 console.log(url);
  return request({
    url: url,
    method: 'get',
  
  });
}
// 获取分类
export function findtype(query) {
  return request({
    url: '/api/v2/category/list',
    method: 'get',
    params: query,
  });
}
export function fetchArticle(id) {
  return request({
    url: '/article/detail',
    method: 'get',
    params: { id },
  });
}

export function fetchPv(pv) {
  return request({
    url: '/article/pv',
    method: 'get',
    params: { pv },
  });
}

export function createArticle(data) {
  return request({
    url: '/article/create',
    method: 'post',
    data,
  });
}

export function updateArticle(data) {
  return request({
    url: '/article/update',
    method: 'post',
    data,
  });
}
