import request from '@/utils/request'

export function getProvinceList(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/province/list/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function provinceGetList() {
  const url ='/api/v2/province/list/10000/1'
  return request({
    url: url,
    method: 'get'
  })
}
export function create(data) {
  return request({
    url: '/api/v2/province/create',
    method: 'post',
    data
  })
}
export function getprovinceinfo(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/province/info/'+params;
  return request({
    url: url,
    method: 'get',
  
  })
}
export function upadte(data,id) {
  const url ='/api/v2/province/update/'+id;
  return request({
    url: url,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/province/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}
