import request from '@/utils/request'
import global from '../global';
import Cookies from 'js-cookie'
console.log(global);
const versions=global.video;
let  token=JSON.parse(Cookies.get('userinfo'));
let access_token=token.authToken.accessToken;
export function getList(params) {
  const url = versions+'/video/get_comment_list/?api_token=api_xiangmu&access_token='+access_token+'&user_id='+token.userId+'&page='+params.page
  return request({
    url: url,
    method: 'get',
  })
}

export function get(id) {
  console.log(id)
  return request({
    url: '/api/v2/store/info/' + id,
    method: 'get'
  })
}

export function getStoreInfo(id) {
  console.log(id)
  return request({
    url: '/api/v2/store/info/' + id,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/api/v2/store/create',
    method: 'post',
    data
  })
}

export function modify(data, id) {
  return request({
    url: '/api/v2/store/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/store/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}
