import request from '@/utils/request'

export function getList(params) {
  console.log("参数");
  console.log(params);
  const url = '/api/v2/operation/list/order/'+params.id+'/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function getManagerList() {
  const url = '/api/v2/manager/list/'
  return request({
    url: url,
    method: 'get',
  })
}
export function get(id) {
  console.log(id)
  return request({
    url: '/api/v2/operation/info/' + id,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/api/v2/operation/create',
    method: 'post',
    data
  })
}

export function modify(data, id) {
  return request({
    url: '/api/v2/operation/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id) {
    return request({
      url: '/api/v2/operation/delete/'+id,
      method: 'post',
    })
  }