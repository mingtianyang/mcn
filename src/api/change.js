import request from '@/utils/request'

// export function getList(params) {
//   return request({
//     url: 'http://www.miaodou.club/api/v2/change/list/' + params.userId,
//     method: 'get',
//   })
// }

export function getList(userId, size, page) {
  return request({
    url: '/api/v2/change/list/' + userId + '/' + size + '/' + page,
    method: 'get'
  })
}

export function get(id) {
  console.log(id)
  return request({
    url: '/api/v2/change/info/' + id,
    method: 'get'
  })
}
export function deleteby(id) {
  console.log(id)
  return request({
    url: '/api/v2/change/delete/' + id,
    method: 'post'
  })
}

export function update(id) {
  console.log(id)
  return request({
    url: '/api/v2/change/update' + id,
    method: 'post'
  })
}

export function post(params) {
  console.log(data)
  const data = {
    userId: params.userId,
    orderId: params.orderId,
    gift: params.gift
  }
  return request({
    url: '/api/v2/change/create',
    method: 'post',
    data
  })
}

// export function finish(params) {
//   console.log(data)
//   const data = {
//     orderId: params.orderId,
//     message: '已取货',
//     status: params.status
//   }
//   return request({
//     url: 'http://www.miaodou.club/api/v2/operation/create',
//     method: 'post',
//     data
//   })
// }

