import request from '@/utils/request'

export function getpurchaseList(id,params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/stock/list/store/'+id+'/'+params.size+'/'+params.page;
  return request({
    url: url,
    method: 'get',
  })
}
// serchbysingle
export function serchbygoods(name,params){
  const url ='/api/v2/goods/search/'+name+"/"+params.size+'/'+params.page;
  return request({
    url: url,
    method: 'get',
  })
}
export function serchbysingle(storeId,id,params) {
 
  const url ='/api/v2/stock/list/store/goods/'+storeId+"/"+id+'/'+params.size+'/'+params.page;
  return request({
    url: url,
    method: 'get',
  })
}
export function getgoodsList(params) {
  console.log(params);
  const url = '/api/v2/goods/list/sale'
  return request({
    url: url,
    method: 'get',
  })
}
export function getstoreList(params) {
  console.log(params);
  const url = '/api/v2/store/list/all'
  return request({
    url: url,
    method: 'get',
  })
}
export function confirmedInbound(id) {
  const url = '/api/v2/stock/confirm/inbound/'+id;
  return request({
    url: url,
    method: 'get',
  })
}
export function confirmedOutbound(id) {
  const url = '/api/v2/stock/confirm/outbound/'+id;
  return request({
    url: url,
    method: 'get',
  })
}
export function create(data) {
  return request({
    url: '/api/v2/stock/create',
    method: 'post',
    data
  })
}
export function createOutbound(data) {
  return request({
    url: '/api/v2/stock/create/outbound',
    method: 'post',
    data
  })
}
export function createInbound(data) {
  return request({
    url: '/api/v2/stock/create/inbound',
    method: 'post',
    data
  })
}
export function updatepurchase(data,id) {
  const url ='/api/v2/stock/update/'+id;
  return request({
    url: url,
    method: 'post',
    data
  })
}

export function deleteby(id){
  const url ='/api/v2/stock/delete/'+id;
  return request({
    url: url,
    method: 'post',
  })
}
export function getpurchaseinfo(id) {
  const url ='/api/v2/stock/info/voucher/'+id;
  return request({
    url: url,
    method: 'get',
  
  })
}
export function purchaseinfo(id) {
  const url ='/api/v2/stock/info/'+id;
  return request({
    url: url,
    method: 'get',
  
  })
}

