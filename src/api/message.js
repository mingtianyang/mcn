import request from '@/utils/request'
import global from '../global';
console.log(global);
const versions=global.user;
import Cookies from 'js-cookie'
let  token=JSON.parse(Cookies.get('userinfo'));
let access_token=token.authToken.accessToken;
console.log(access_token);
export function getList(params,data) {
  console.log(params)
  const url = versions+'/member/follow_list/?api_token=api_xiangmu&access_token='+data.authToken.accessToken+'&user_id='+data.userId
  return request({
    url: url,
    method: 'get',
  })
}
export function getFansList(params,data) {
  console.log(params)
  const url = versions+'/member/fans_list/?api_token=api_xiangmu&access_token='+data.authToken.accessToken+'&user_id='+data.userId
  return request({
    url: url,
    method: 'get',
  })
}
export function BrandGetList() {
  const url ='/api/v2/message/list/10000/1'
  return request({
    url: url,
    method: 'get'
  })
}
export function getUserList(params) {
    console.log("获取参数");
    console.log(params);
    const url ='/api/v2/user/list/10000/1';
    return request({
      url: url,
      method: 'get',
    })
  }
export function create(data) {
  return request({
    url: '/api/v2/message/create',
    method: 'post',
    data
  })
}
export function getmessageinfo(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/message/info/'+params;
  return request({
    url: url,
    method: 'get',
  
  })
}
export function upadte(data,id) {
  const url ='/api/v2/message/update/'+id;
  return request({
    url: url,
    method: 'post',
    data
  })
}
export function deleteby(id,sdata){
  let data={
    'api_token':'api_xiangmu',
    'access_token':sdata.authToken.accessToken,
    'user_id':sdata.userId,
    'follow_user_id':id,
    'follow_type':2,
    
  }
  const url =versions+'/member/focus_user/';
  return request({
    url: url,
    method: 'post',
   data
  })
}
export function fansfoucus(id,sdata){
  let data={
    'api_token':'api_xiangmu',
    'access_token':sdata.authToken.accessToken,
    'user_id':sdata.userId,
    'follow_user_id':id,
    'follow_type':1,
    
  }
  const url =versions+'/member/focus_user/';
  return request({
    url: url,
    method: 'post',
   data
  })
}
