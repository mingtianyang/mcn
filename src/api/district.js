import request from '@/utils/request'

export function getDistrictList(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/district/list/'+params.cityId+'/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function districtGetList() {
  const url ='/api/v2/district/list/10000/1'
  return request({
    url: url,
    method: 'get'
  })
}
export function create(data) {
  return request({
    url: '/api/v2/district/create',
    method: 'post',
    data
  })
}
export function getdistrictinfo(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/district/info/'+params;
  return request({
    url: url,
    method: 'get',
  
  })
}
export function update(data,id) {
  const url ='/api/v2/district/update/'+id;
  return request({
    url: url,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/district/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}
