import request from '@/utils/request'

export function getsnapshotList(id,params) {
  const url = '/api/v2/snapshot/list/'+id+'/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function GoodsGetList() {
  console.log("获取参数");
  const url ='/api/v2/snapshot/list'
  // const url='/api/v2/tag';
  return request({
    url: url,
    method: 'get',
    
  })
}
export function get(id) {
  console.log(id)
  return request({
    url: '/api/v2/snapshot/info/' + id,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/api/v2/snapshot/create',
    method: 'post',
    data
  })
}

export function modify(data, id) {
  return request({
    url: '/api/v2/snapshot/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/snapshot/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}
