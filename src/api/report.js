import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/api/v2/change/list/' + params.userId,
    method: 'get',
  })
}

export function getRechargeListByPagination(startDate, endDate, size, page) {
  return request({
    url: '/api/v2/report/recharge/pagination/' + startDate + '/' + endDate + '/' + size + '/' + page,
    method: 'get'
  })
}

export function getGoodsListByPagination(startDate, endDate, size, page) {
    return request({
      url: '/api/v2/report/goods/pagination/' + startDate + '/' + endDate + '/' + size + '/' + page,
      method: 'get'
    })
  }
  


