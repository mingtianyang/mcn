import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/api/v2/order',
    method: 'get',
    params
  })
}

export function getListByPagination(storeid,type,status,size, page) {
  return request({
    url: '/api/v2/order/list/store/' + storeid +"/"+type+ "/"+ status + '/' + size + '/' + page,
    method: 'get',
  })
}
export function getserch(storeid,data) {
  return request({
    url: '/api/v2/order/list/number/' + storeid +"/"+data,
    method: 'get',
  })
}
export function PaymentInquiry(payBy,number) {
  return request({
    url: '/api/v2/payment/'+payBy+'/query/' + number,
    method: 'post',
  })
}
export function getStoreList(store, status) {
  return request({
    url: '/api/v2/order/store/' + store + '/1/' + status,
    method: 'get'
  })
}

export function getStoreListByPagination(store, status, size, page) {
  
  return request({
    url: '/api/v2/order/list/store/' + store + '/1/' + status + '/' + size + '/' + page,
    method: 'get'
  })
}

export function deleteby(id) {
  return request({
    url: '/api/v2/order/delete/'+id,
    method: 'post',
  })
}

// export function create(data) {
//   return request({
//     url: '/api/v2/order/create',
//     method: 'post',
//     data
//   })
// }

export function getinfo(id) {
  console.log(id)
  return request({
    url: '/api/v2/order/info/' + id,
    method: 'get'
  })
}
export function update(data,id) {
  console.log(id)
  return request({
    url: '/api/v2/order/update/' + id,
    method: 'post',
    data
  })
}
export function operation(params) {
  console.log(data)
  const data = {
    orderId: params.orderId,
    message: '未取货，货架：' + params.storage + '-' + params.number,
    status: params.status
  }
  return request({
    url: '/api/v2/operation/create',
    method: 'post',
    data
  })
}

export function finish(params) {
  console.log(data)
  const data = {
    orderId: params.orderId,
    message: '已取货',
    status: params.status
  }
  return request({
    url: '/api/v2/operation/create',
    method: 'post',
    data
  })
}

