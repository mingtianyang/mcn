import request from '@/utils/request'

export function getList(params) {
  const url = '/api/v2/goods/list/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function goodssearch(name,params) {
  const url = '/api/v2/goods/search/'+name+"/"+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function GoodsGetList() {
  console.log("获取参数");
  const url ='/api/v2/goods/list'
  // const url='/api/v2/tag';
  return request({
    url: url,
    method: 'get',
    
  })
}
export function get(id) {
  console.log(id)
  return request({
    url: '/api/v2/goods/detail/' + id,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/api/v2/goods/create',
    method: 'post',
    data
  })
}

export function modify(data, id) {
  return request({
    url: '/api/v2/goods/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/goods/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}
