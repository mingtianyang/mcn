import request from '@/utils/request'

export function getList(params) {
  const url = '/api/v2/category/list/'+params.parentId+'/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function CategoryGetList() {
  const url = '/api/v2/category/list/0/10000/1'
  return request({
    url: url,
    method: 'get',
  })
}
export function create(data) {
  return request({
    url: '/api/v2/category/create',
    method: 'post',
    data
  })
}

export function get(id) {
  console.log(id)
  return request({
    url: '/api/v2/category/info/' + id,
    method: 'get'
  })
}

export function modify(data,id) {
  return request({
    url: '/api/v2/category/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/category/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}

