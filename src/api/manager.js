import request from '@/utils/request'

export function getList(params) {
  const url = '/api/v2/manager/list/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function getinfo(id) {
  console.log("请求管理员")
  return request({
    url: '/api/v2/manager/info/' + id,
    method: 'get'
  })
}

export function getinfoManager() {
  console.log("请求管理员")
  return request({
    url: '/api/v2/manager/info',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/api/v2/manager/create',
    method: 'post',
    data
  })
}

export function modify(data, id) {
  return request({
    url: '/api/v2/manager/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id) {
    return request({
      url: '/api/v2/manager/delete/'+id,
      method: 'post',
    })
  }