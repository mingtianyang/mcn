import request from '@/utils/request'
import Cookies from 'js-cookie'
import global from '../global';
console.log(global);
const versions=global.video;
const userverison=global.user;
let  token=JSON.parse(Cookies.get('userinfo'));
let access_token=token.authToken.accessToken;
console.log(token);
export function getList(params,state,data) {
  console.log(data)
  const url = versions+'/video/mymcn_video_list/?api_token=api_xiangmu&access_token='+data.authToken.accessToken+'&user_id='+data.userId+'&page='+params.page+"&state="+status
  return request({
    url: url,
    method: 'get',
  })
}
export function getplayer(e,data){
  const url = versions+'/video_api/get_video_play_auth/?api_token=api_xiangmu&access_token='+data.authToken.accessToken+'&user_id='+data.userId+'&play_id='+e
  return request({
    url: url,
    method: 'get',
  })
}
export function gettypeList(params,data) {
  console.log(params)
  const url = versions+'/video/get_video_class/?api_token=api_xiangmu&access_token='+data.authToken.accessToken;
  return request({
    url: url,
    method: 'get',
  })
}
export function gettagList(params,data) {
  const url = versions+'/video/get_video_class_label/?api_token=api_xiangmu&access_token='+data.authToken.accessToken+'&pid='+params
  return request({
    url: url,
    method: 'get',
  })
}
export function getcommonList(params,data) {
  const url = versions+'/video/get_comment_list/?api_token=api_xiangmu&access_token='+data.authToken.accessToken+'&user_id='+data.userId+'&play_id='+params
  return request({
    url: url,
    method: 'get',
  })
}
export function get(id) {
  console.log(id)
  return request({
    url: '/api/v2/video/info/' + id,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/api/v2/video/create',
    method: 'post',
    data
  })
}
export function gettokens(data) {
  console.log(data);
  return request({
    url: versions+'/video_api/get_create_upload_video',
    method: 'post',
    data
  })
}
//获取图片上传临时权限
export function getststokens(data) {
  console.log(data);
  return request({
    url: userverison+'/member/get_oss_ststoken/?api_token=api_xiangmu&access_token='+data.authToken.accessToken+'&user_id='+data.userId,
    method: 'get',

  })
}
export function videorequest(id,data) {
  console.log(id)
  return request({
    url: versions+'/video_api/get_refresh_upload_video_request/?api_token=api_xiangmu&access_token='+data.authToken.accessToken+'&user_id='+data.userId+"&video_id="+ id,
    method: 'get'
  })
}
export function videorepeatrequest(id,data) {
  console.log(id)
  return request({
    url: versions+'/video/get_reply_list/?api_token=api_xiangmu&access_token='+data.authToken.accessToken+'&user_id='+data.userId+"&comment_id="+ id,
    method: 'get'
  })
}
export function modify(data, id) {
  return request({
    url: '/api/v2/video/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id,item) {
  let data={
    'api_token':'api_xiangmu',
    'access_token':item.authToken.accessToken,
    'user_id':item.userId,
    'play_id':id,
  }
    return request({
      url: versions+'/video/delete_video_play/',
      method: 'post',
      data
    })
  }
  //删除评论
  export function commentdelete(id,item){
    let data={
      'api_token':'api_xiangmu',
      'access_token':item.authToken.accessToken,
      'user_id':item.userId,
      'comment_id':id,
    }
    const url =versions+'/video/cancel_comment';
    return request({
      url: url,
      method: 'post',
      data
    })
  }
  //删除回复
  export function repeatedelete(id,sdata){
    let data={
      'api_token':'api_xiangmu',
      'access_token':sdata.authToken.accessToken,
      'user_id':sdata.userId,
      'reply_id':id,
    }
    const url =versions+'/video/cancel_comment_reply';
    return request({
      url: url,
      method: 'post',
      data
    })
  }
   //视频评论
   export function videocomment(item,sdata){
    let data={
      'api_token':'api_xiangmu',
      'access_token':sdata.authToken.accessToken,
      'user_id':sdata.userId,
      'play_id':item.play_id,
      'content':item.content
    }
    const url =versions+'/video/add_comment';
    return request({
      url: url,
      method: 'post',
      data
    })
  }
  //评论回复
  export function commentback(item,sdata){
    let data={
      'api_token':'api_xiangmu',
      'access_token':sdata.authToken.accessToken,
      'user_id':sdata.userId,
      'play_id':item.play_id,
      'r_uid':item.r_uid,
      'comment_id':item.comment_id,
      'content':item.content
    }
    const url =versions+'/video/add_comment_reply';
    return request({
      url: url,
      method: 'post',
      data
    })
  }
  //评论点赞
  export function commentpickup(item,sdata){
    let data={
      'api_token':'api_xiangmu',
      'access_token':sdata.authToken.accessToken,
      'user_id':sdata.userId,
      'comment_id':item,
    }
    const url =versions+'/video/add_comment_support';
    return request({
      url: url,
      method: 'post',
      data
    })
  }
  //评论点赞取消
  export function canclecommentpickup(item,sdata){
    let data={
      'api_token':'api_xiangmu',
      'access_token':sdata.authToken.accessToken,
      'user_id':sdata.userId,
      'comment_id':item,
    }
    const url =versions+'/video/cancel_comment_support';
    return request({
      url: url,
      method: 'post',
      data
    })
  }
   //评论回复点赞
   export function replapickup(item,sdata){
    let data={
      'api_token':'api_xiangmu',
      'access_token':sdata.authToken.accessToken,
      'user_id':sdata.userId,
      'reply_id':item,
    }
    const url =versions+'/video/add_reply_support';
    return request({
      url: url,
      method: 'post',
      data
    })
  }
  //取消评论回复点赞
  export function canclereplapickup(item,sdata){
    let data={
      'api_token':'api_xiangmu',
      'access_token':sdata.authToken.accessToken,
      'user_id':sdata.userId,
      'reply_id':item,
    }
    const url =versions+'/video/cancel_reply_support';
    return request({
      url: url,
      method: 'post',
      data
    })
  }
  