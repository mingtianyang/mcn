import request from '@/utils/request'

export function getList(params) {
  const url = '/api/v2/collection/list/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}

export function getinfo(id) {
  console.log(id)
  return request({
    url: '/api/v2/collection/info/' + id,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/api/v2/collection/create',
    method: 'post',
    data
  })
}

export function modify(data, id) {
  return request({
    url: '/api/v2/collection/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/collection/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}
