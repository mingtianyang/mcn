import request from '@/utils/request'

export function getList(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/brand/list/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function BrandGetList() {
  const url ='/api/v2/brand/list/10000/1'
  return request({
    url: url,
    method: 'get'
  })
}
export function create(data) {
  return request({
    url: '/api/v2/brand/create',
    method: 'post',
    data
  })
}
export function getbrandinfo(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/brand/info/'+params;
  return request({
    url: url,
    method: 'get',
  
  })
}
export function upadte(data,id) {
  const url ='/api/v2/brand/update/'+id;
  return request({
    url: url,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/brand/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}
