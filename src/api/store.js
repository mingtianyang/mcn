import request from '@/utils/request'

export function getList(params) {
  console.log(params);
  let myurl=''
  if(params.size&&params.page){
    myurl = '/api/v2/store/list/store/'+params.size+'/'+params.page
  }else{
    myurl= '/api/v2/store/list/store'
  }
  
  return request({
    url: myurl,
    method: 'get',
  })
}
export function getparentList(params) {
  console.log(params);
  let myurl=''
  if(params.size&&params.page){
    myurl = '/api/v2/store/list/warehouse/'+params.size+'/'+params.page
  }else{
    myurl= '/api/v2/store/list/warehouse'
  }
  
  return request({
    url: myurl,
    method: 'get',
  })
}
export function get(id) {
  console.log(id)
  return request({
    url: '/api/v2/store/info/' + id,
    method: 'get'
  })
}

export function getStoreInfo(id) {
  console.log(id)
  return request({
    url: '/api/v2/store/info/' + id,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/api/v2/store/create',
    method: 'post',
    data
  })
}

export function modify(data, id) {
  return request({
    url: '/api/v2/store/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/store/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}
