import request from '@/utils/request'

export function getList(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/tag/list/'+params.size+'/'+params.page
  // const url='/api/v2/tag';
  return request({
    url: url,
    method: 'get',
  })
}
export function TagGetList() {
  console.log("获取参数");
  const url ='/api/v2/tag/list/10000/1'
  // const url='/api/v2/tag';
  return request({
    url: url,
    method: 'get',
    
  })
}
export function create(data) {
  return request({
    url: '/api/v2/tag/create',
    method: 'post',
    data
  })
}
export function getinfo(data) {
  const url ='/api/v2/tag/info/'+data;
  // const url='/api/v2/tag';
  return request({
    url: url,
    method: 'get',
    
  })
}
export function update(data,id) {
  const url ='/api/v2/tag/update/'+id;
  return request({
    url: url,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/tag/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}