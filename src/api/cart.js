import request from '@/utils/request'

export function getList(params) {
  const url = '/api/v2/cart/list/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function get(id) {
  console.log(id)
  return request({
    url: '/api/v2/cart/info/' + id,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/api/v2/cart/create',
    method: 'post',
    data
  })
}

export function modify(data, id) {
  return request({
    url: '/api/v2/cart/update/'+id,
    method: 'post',
    data
  })
}
export function deleteby(id) {
    return request({
      url: '/api/v2/cart/delete/'+id,
      method: 'post',
    })
  }