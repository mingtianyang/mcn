import request from '@/utils/request'

export function getCityList(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/city/list/'+params.provinceId+'/'+params.size+'/'+params.page
  return request({
    url: url,
    method: 'get',
  })
}
export function cityGetList() {
  const url ='/api/v2/city/list/10000/1'
  return request({
    url: url,
    method: 'get'
  })
}
export function create(data) {
  return request({
    url: '/api/v2/city/create',
    method: 'post',
    data
  })
}
export function getcityinfo(params) {
  console.log("获取参数");
  console.log(params);
  const url ='/api/v2/city/info/'+params;
  return request({
    url: url,
    method: 'get',
  
  })
}
export function update(data,id) {
  const url ='/api/v2/city/update/'+id;
  return request({
    url: url,
    method: 'post',
    data
  })
}
export function deleteby(id){
  const url ='/api/v2/city/delete/'+id;
  return request({
    url: url,
    method: 'post',
   
  })
}
