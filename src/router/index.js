import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-attestation-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['attestation','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    meta: {
      roles: ['attestation', 'artist','employee'],
    },
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },

  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
    meta: {
      roles: ['attestation', 'artist','employee'],
    },
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true,
    meta: {
      roles: ['attestation', 'artist','employee'],
    },
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true,
    meta: {
      roles: ['attestation', 'artist','employee'],
    },
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true,
    meta: {
      roles: ['attestation', 'artist','employee'],
    },
  },
  //出货
  // {
  //   path: '/',
  //   component: Layout,
  //   redirect: '/SalesDesk',
  //   name: '出货',
  //   hidden:true,
  //   meta: {
  //     title: '出货',
  //     icon: 'pc-Ship-w',
  //     roles: ['attestation', 'artist','employee'],
  //   },
  //   children: [
  //     {
  //       path: 'SalesDesk',
  //       component: () => import('@/views/Shipment/SalesDesk'),
  //       name: 'purchase',
  //       meta: {
  //         title: '售货台',
  //         // or you can only set roles in sub nav
  //       }
  //     },
  //     {
  //       path: 'PendingOrder',
  //       component: () => import('@/views/Shipment/PendingOrder/PendingOrder'),
  //       name: '未付款订单',
  //       meta: {
  //         title: '未付款订单'
  //       },
  //     },
  //     {
  //       path:'OrderSettlement',
  //       component: () => import('@/views/Shipment/PendingOrder/OrderSettlement'),
  //       hidden:true,
  //       name: 'OrderSettlement',
  //       meta: {
  //         title: '未付款订单 / 收款'
  //       }
  //     },
  //     {
  //       path: 'OrderCompleted',
  //       component: () => import('@/views/Shipment/OrderCompleted/OrderCompleted'),
  //       name: 'OrderCompleted',
  //       meta: {
  //         title: '已取货订单',
         
  //       }
  //     }
  //   ]
  // },
  // {
  //   path: '/',redirect: '/home',
  //   component: Layout
  //   },
   {
    path: '/',
    component: Layout,
    redirect: '/home',
    name: 'home',
   
    meta: {
      title: '我的',
      icon: 'pc-Ship-w',
      
    },
    children: [
      {
        path: 'home',
        component: () => import('@/views/home/home'),
        name: 'home',
        meta: {
          title: '我的',
          // or you can only set roles in sub nav
        }
      },
     
    ]
  },
 
  // 库存
  {
    path: '/stock',
    component: Layout,
    redirect: '/purchase',
    name: '库存',
     hidden:true,
    meta: {
      title: '库存',
      icon: 'pc-inStock-w',
      roles: ['attestation','artist'],
      // you can set roles in root nav
    },
 
    children: [
      {
        path: 'purchase',
        component: () => import('@/views/stock/purchase'),
        // hidden:true,
        name: 'purchase',
        meta: {
          title: '库存',
          roles: ['attestation','artist'],
          // or you can only set roles in sub nav
        },
      },
      {
        path: 'purchasecreate',
        component: () => import('@/views/stock/form/purchasecreate'),
        name: 'PurchaseCreate',
        hidden:true,
        meta: {
          title: '创建库存',
          roles: ['attestation','artist'],
          // or you can only set roles in sub nav
        },
      },
      {
        path: 'outBound',
        component: () => import('@/views/stock/form/outBound'),
        name: 'OutBound',
        hidden:true,
        meta: {
          title: '出库',
          roles: ['attestation','artist'],
          // or you can only set roles in sub nav
        },
      },
      {
        path: 'purchaseupdate:id',
        component: () => import('@/views/stock/form/purchaseupdate'),
        name: 'PurchaseUpdate',
        hidden:true,
        meta: {
          roles: ['attestation','artist'],
          title: '编辑库存',
          // or you can only set roles in sub nav
        },
      },
      {
        path: 'purchasesubmit:id',
        component: () => import('@/views/stock/form/purchasesubmit'),
        name: 'PurchaseSubmit',
        hidden:true,
        meta: {
          roles: ['attestation','artist'],
          title: '入库',
          // or you can only set roles in sub nav
        },
      },
      {
        path: 'warehouse',
        component: () => import('@/views/stock/warehouse'),
        name: '返仓发单',
        hidden:true,
        meta: {
          roles: ['attestation','artist'],
          title: '返仓发单'
          // if do not set roles, means: this page does not require permission
        }
      },
      {
        path: 'consume',
        component: () => import('@/views/stock/consume'),
        name: '货品消耗',
        hidden:true,
        meta: {
          title: '货品消耗',
          roles: ['attestation','artist'],
        }
      },
      {
        path: 'inventory',
        component: () => import('@/views/stock/inventory'),
        name: '库存盘点',
        hidden:true,
        meta: {
          title: '库存盘点',
          roles: ['attestation','artist'],
        }
      }
    ]
  },
  //货单
  {
    path: '/manifest',
    component: Layout,
    hidden:true,
    redirect: '/manifest/pickUp',
    name: 'manifest',
    meta: { title: '订单', icon: 'form' ,  roles: ['attestation', 'artist'],},
    children: [
      {
        path: 'sorting',
        name: 'manifestSorting',
        component: () => import('@/views/manifest/sorting/index'),
        meta: { title: '分拣列表', icon: 'documentation', roles: ['attestation','artist']}
      },
      {
        path: 'form',
        name: 'goToSorting',
        hidden:true,
        component: () => import('@/views/manifest/form/index'),
        meta: { title: '货单分拣', roles: ['attestation','artist']}
      },
      {
        path: 'pickUp',
        name: 'manifestPickUp',
        component: () => import('@/views/manifest/pickUp/index'),
        meta: { title: '提货列表', icon: 'tab' , roles: ['attestation','artist']}
      },
      {
        path: 'list',
        component: () => import('@/views/order/list'),
        name: '全部订单',
        meta: {
          title: '全部订单',
          icon: 'money' , 
          roles: ['attestation', 'artist'],
          // or you can only set rolemployeees in sub nav
        }
      },
    ]
  },
  //货仓
  {
    path: '/warehouse',
    //hidden:true,
    component: Layout,
    hidden:true,
    redirect: '/warehouse/list',
    name: 'warehouse',
    meta: { title: '评论管理', icon: 'form' ,  roles: ['attestation', 'artist'],},
    children: [
      // {
      //   path: 'list',
      //   name: 'warehouselist',
      //   component: () => import('@/views/warehouse/list/index'),
      //   meta: { title: '货仓总览', icon: 'documentation', roles: ['attestation','artist']}
      // },
      // {
      //   path: 'form',
      //   name: 'goToSorting',
      //   component: () => import('@/views/warehouse/inventory/index'),
      //   meta: { title: '货仓盘点', icon: 'documentation',roles: ['attestation','artist']}
      // },
      // {
      //   path: 'modify',
      //   name: 'warehousePickUp',
      //   component: () => import('@/views/warehouse/allocate/index'),
      //   meta: { title: '货仓调拨', icon: 'tab' , roles: ['attestation','artist']}
      // },
      {
        path: 'sorting',
        name: 'manifestSorting',
        component: () => import('@/views/warehouse/stockorder'),
       
        redirect: '/sorting/sortinglist',
        meta: { title: '出入货单', icon: 'documentation', roles: ['attestation','artist'],},
        children:[
         {
              path: 'sortinglist',
              name: 'manifestSorting',
              component: () => import('@/views/warehouse/stockorder/list'),
              meta: { title: '货单列表', icon: 'documentation', roles: ['attestation','artist'],}
          },
          {
            path: 'lookOverSorting',
            name: 'LookOverSorting',
            hidden:true,
            component: () => import('@/views/warehouse/stockorder/lookOver'),
            meta: { title: '查看货单', icon: 'documentation', roles: ['attestation','artist'],}
        },
          {
            path: 'sortingform',
            name: 'manifestSorting',
            hidden:true,
            component: () => import('@/views/warehouse/stockorder/form'),
            meta: { title: '修改货单', icon: 'documentation', roles: ['attestation','artist'],}
        },
           
        ]
        
      },
    
    ]
  },
  //类别管理
  {
    path: '/category',
    component: Layout,
    redirect: '/category/list',
    name: 'Category',
    hidden:true,
    meta: { title: '评论管理', icon: 'clipboard' ,
    roles: ['attestation', 'artist','employee'],
  },
    children: [
      {
        path: 'list',
        name: 'CategoryList',
        component: () => import('@/views/category/list/index'),
        meta: { title: '评论管理', 
        roles: ['attestation', 'artist','employee'],
        icon: 'clipboard' }
      },
      {
        path: 'create',
        name: 'CategoryCreate',
        component: () => import('@/views/category/form/index'),
        hidden:true,
        meta: { title: '添加类别', 
        roles: ['attestation', 'artist','employee'], }
      },
      {
        path: 'edit:id',
        hidden: true,
        name: 'CategoryModify',
        component: () => import('@/views/category/modify'),
        hidden:true,
        meta: { title: '编辑类别',
        roles: ['attestation', 'artist','employee'],}
      }
    ]
  },
  //品牌管理
  {
    path: '/brand',
    component: Layout,
    redirect: '/brand/list',
    name: 'Brand',
    hidden:true,
    meta: { title: '品牌管理', icon: 'guide' ,
    roles: ['attestation', 'artist','employee'],},
    children: [
      {
        path: 'list',
        name: 'BrandList',
        component: () => import('@/views/brand/list/index'),
        meta: { title: '品牌管理',
        roles: ['attestation', 'artist','employee'],
        icon: 'guide' }
      },
      {
        path: 'create',
        name: 'BrandCreate',
        component: () => import('@/views/brand/form/index'),
        hidden:true,
        meta: { title: '添加品牌',
        roles: ['attestation', 'artist','employee'],}
      },
      {
        path: 'update:id',
        name: 'BrandUpdate',
        hidden:true,
        component: () => import('@/views/brand/form/edit'),
        hidden:true,
        meta: { title: '修改品牌',
        roles: ['attestation', 'artist','employee'], }
      }
    ]
  },
//标签管理
  {
    path: '/tag',
    component: Layout,
    hidden:true,
    redirect: '/tag/list',
    name: 'Tag',
    meta: { title: '标签管理', icon: 'tab' ,
    roles: ['attestation', 'artist','employee'],},
    children: [
      {
        path: 'list',
        name: 'TagList',
        component: () => import('@/views/tag/list/index'),
        meta: { title: '标签管理', 
        roles: ['attestation', 'artist','employee'],
        icon: 'tab' 
        }
      },
      {
        path: 'create',
        name: 'TagCreate',
        component: () => import('@/views/tag/form/index'),
        hidden:true,
        meta: { title: '添加标签', 
        roles: ['attestation', 'artist','employee'],}
      },
      {
        path: 'edit:id',
        name: 'TagEdit',
        hidden:true,
        component: () => import('@/views/tag/form/edit'),
        hidden:true,
        meta: { title: '编辑标签', 
        roles: ['attestation', 'artist','employee'], }
      }
    ]
  },
  //商品管理
  {
    path: '/goods',
    component: Layout,
    hidden:true,
    redirect: '/goods/list',
   // will always show the root menu
    name: '商品管理',
    meta: {
      title: '商品管理',
      icon: 'example',
      roles: ['attestation', 'artist','employee'],
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/goods/list'),
        name: '商品管理',
        meta: {
          title: '商品管理',
          icon: 'example',
          roles: ['attestation', 'artist','employee'],
          // or you can only set roles in sub nav
        }
      },
      {
        path: 'form',
        component: () => import('@/views/goods/form'),
        hidden:true,
        name: 'form',
        meta: {
          title: '添加商品',
          roles: ['attestation', 'artist','employee'],
        },
      },
      {
        path: 'edit:id',
        hidden: true,
        name: 'GoodsModify',
        component: () => import('@/views/goods/modify'),
        hidden:true,
        meta: { title: '编辑商品',
        roles: ['attestation', 'artist','employee'],}
      }
    ]
  },
  //店铺管理
  {
    path: '/store',
    component: Layout,
    hidden:true,
    redirect: '/store/list',
     // will always show the root menu
    name: '店铺管理',
    meta: {
      title: '店铺管理',
      icon: 'documentation',
      roles: ['attestation', 'artist'],
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/store/list'),
        name: '店铺管理',
        meta: {
          title: '店铺管理',
          icon: 'documentation',
          roles: ['attestation', 'artist'],
          // or you can only set roles in sub nav
        }
      },
      {
        path: 'form',
        component: () => import('@/views/store/form'),
        hidden:true,
        name: 'addStore',
        meta: {
          title: '添加店铺',
          roles: ['attestation', 'artist'],
        },
      },
      {
        path: 'edit:id',
        hidden: true,
        name: 'storeModify',
        component: () => import('@/views/store/modify'),
        hidden:true,
        meta: { title: '编辑店铺',
        roles: ['attestation', 'artist'],}
      }
    ]
  },
  //商品集合
  {
    path: '/collection',
    component: Layout,
    hidden:true,
    redirect: '/collection/list',
     // will always show the root menu
    name: '商品集合管理',
    meta: {
      title: '商品集合管理',
      icon: 'component',
      roles: ['attestation', 'employee'],
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/collection/collectionlist'),
        name: '商品集合管理',
        meta: {
          title: '商品集合管理',
          icon: 'component',
          roles: ['attestation', 'employee'],
          // or you can only set roles in sub nav
        }
      },
      {
        path: 'form',
        component: () => import('@/views/collection/collectioncreate'),
        hidden:true,
        name: '添加商品集合',
        meta: {
          title: '添加商品集合',
          roles: ['attestation', 'employee'],
        },
      },
      {
        path: 'edit:id',
        hidden: true,
        name: 'collectionUpdate',
        component: () => import('@/views/collection/collectionupdate'),
        hidden:true,
        meta: { title: '编辑商品集合',
        roles: ['attestation', 'employee'],}
      }
    ]
  },
  //商品快照
  {
    path: '/snapshot',
    component: Layout,
    redirect: '/snapshot/list',
    hidden: true,
    // hidden:true,
    // hidden:true,
     // will always show the root menu
    name: '商品快照管理',
    meta: {
      title: '商品快照管理',
      icon: 'documentation',
      roles: ['attestation','artist'],
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/snapshot/snapshotlist'),
        name: 'snapshotlist',
        meta: {
          title: '商品快照管理',
          icon: 'documentation',
          roles: ['attestation','artist'],
          // or you can only set roles in sub nav
        }
      },
      {
        path: 'form',
        component: () => import('@/views/snapshot/snapshotcreate'),
        hidden:true,
        name: '添加商品快照',
        meta: {
          title: '添加商品快照',
          roles: ['attestation','artist'],
        },
      },
      {
        path: 'edit:id',
        hidden: true,
        name: 'snapshotUpdate',
        component: () => import('@/views/snapshot/snapshotupdate'),
        hidden:true,
        meta: { title: '编辑商品快照',
        roles: ['attestation', 'artist',],}
      }
    ]
  },
  //视频管理
  {
    path: '/video',
    component: Layout,
    redirect: '/video/list',
     // will always show the root menu
    name: '视频管理',
    // hidden:true,
    alwaysShow: true,
    meta: {
      title: '管理中心',
      icon: 'pc-manifest-w',
      
      roles: ['attestation', 'artist','employee'],
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/video/list'),
        name: 'video',
       
        meta: {
          title: '内容管理',
          icon: 'pc-manifest-w',
          roles: ['attestation', 'artist','employee'],
          // or you can only set roles in sub nav
        }
      },
      {
        path: 'foucuslist',
        name: 'BrandList',
        component: () => import('@/views/message/list/index'),
        roles: ['attestation', 'artist','employee'],
        meta: { title: '关注管理',
        icon: 'pc-manifest-w',}
      },
      {
        path: 'fanslist',
        name: 'FansList',
        component: () => import('@/views/message/fanslist/index'),
        roles: ['attestation', 'artist','employee'],
        meta: { title: '粉丝管理',
        icon: 'pc-manifest-w',}
      },
      {
        path: 'form',
        component: () => import('@/views/video/form'),
        hidden:true,
        name: '添加视频',
        meta: {
          title: '添加视频',
          roles: ['attestation', 'artist','employee'],
        },
      },
      {
        path: 'edit:id',
        hidden: true,
        name: 'videoModify',
        component: () => import('@/views/video/modify'),
        hidden:true,
        meta: { title: '编辑视频',
        roles: ['attestation', 'artist','employee'],}
      }
    ]
  },
  //艺人管理
  {
    path: '/user',
    component: Layout,
    redirect: '/user/list',
    name: 'User',
    meta: { title: '艺人管理',  icon: 'pc-manifest-w',
    roles: ['attestation'], },
    children: [
      {
        path: 'list',
        name: 'UserList',
        component: () => import('@/views/user/userlist'),
        meta: { title: '艺人管理', 
        icon: 'pc-manifest-w',
        roles: ['attestation'],
        }
      },
      {
        path: 'create',
        name: 'UserCreate',
        component: () => import('@/views/user/usercreate'),
        hidden:true,
        meta: { title: '添加艺人', 
        roles: ['attestation',], }
      },
      {
        path: 'edit:id',
        name: 'UserEdit',
        hidden:true,
        component: () => import('@/views/user/usereditor'),
        hidden:true,
        meta: { title: '编辑艺人', 
        roles: ['attestation',],}
      }
    ]
  },
  //订单管理
  // {
  //   path: '/order',
  //   component: Layout,
    
  //   redirect: '/order/list',
  //    // will always show the root menu
  //   name: '订单管理',
  //   meta: {
  //     title: '订单管理',
  //     icon: 'pc-manifest-w',
  //     roles: ['attestation','artist'],
  //   },
  //   children: [
  //     {
  //       path: 'list',
  //       component: () => import('@/views/order/list'),
  //       name: '订单管理',
  //       meta: {
  //         title: '订单管理',
  //         roles: ['attestation','artist'],
  //         // or you can only set roles in sub nav
  //       }
  //     },
  //     {
  //       path: 'form:id',
  //       component: () => import('@/views/order/form'),
  //       hidden:true,
  //       name: 'orderForm',
  //       meta: {
  //         title: '修改订单',
  //         roles: ['attestation','artist'],
  //       },
  //     },
  //     {
  //       path: 'edit:id',
  //       hidden: true,
  //       name: 'orderModify',
  //       component: () => import('@/views/order/modify'),
  //       hidden:true,
  //       meta: { title: '编辑订单',roles: ['attestation','artist'],}
  //     }
  //   ]
  // },
  
  //省份管理
  {
    path: '/province',
    component: Layout,
    redirect: '/province/list',
    hidden:true,
    name: 'province',
    meta: { title: '省份管理', icon: 'international' ,roles: ['attestation',],},
    children: [
      {
        path: 'list',
        name: 'provinceList',
        component: () => import('@/views/province/list/index'),
        
        meta: { title: '省份管理', roles: ['attestation',],}
      },
      {
        path: 'create',
        name: 'provinceCreate',
        component: () => import('@/views/province/form/index'),
        hidden:true,
        meta: { title: '添加省份',
        roles: ['attestation', ],}
      },
      {
        path: 'update:id',
        name: 'provinceUpdate',
        hidden:true,
        component: () => import('@/views/province/form/edit'),
        hidden:true,
        meta: { title: '修改省份',
        roles: ['attestation',], }
      }
    ]
  },
  //城市管理
  {
    path: '/city',
    component: Layout,
    hidden:true,
    
    redirect: '/city/list',
    name: 'city',
    meta: { title: '城市管理', icon: 'example' ,
    roles: ['attestation',],},
    children: [
      {
        path: 'list:id',
        name: 'cityList',
        component: () => import('@/views/city/list/index'),
        
        meta: { title: '城市管理',  roles: ['attestation',],}
      },
      {
        path: 'create',
        name: 'cityCreate',
        component: () => import('@/views/city/form/index'),
        hidden:true,
        meta: { title: '添加城市',
        roles: ['attestation',],}
      },
      {
        path: 'update:id',
        name: 'cityUpdate',
        hidden:true,
        component: () => import('@/views/city/form/edit'),
        hidden:true,
        meta: { title: '修改城市',
        roles: ['attestation',], }
      }
    ]
  },
  //城区管理
  {
    path: '/district',
    component: Layout,
    hidden:true,
    redirect: '/district/list',
    name: 'district',
    meta: { title: '城区管理', icon: 'example',
    roles: ['attestation'], },
    children: [
      {
        path: 'list/:id/:provinceId',
        name: 'districtList',
        component: () => import('@/views/district/list/index'),
        
        meta: { title: '城区管理', }
      },
      {
        path: 'create',
        name: 'districtCreate',
        component: () => import('@/views/district/form/index'),
        hidden:true,
        meta: { title: '添加城区',
        roles: ['attestation',],}
      },
      {
        path: 'update:id',
        name: 'districtUpdate',
        hidden:true,
        component: () => import('@/views/district/form/edit'),
        hidden:true,
        meta: { title: '修改城区',
        roles: ['attestation', ], }
      }
    ]
  },
  //消息管理
  {
    path: '/message',
    component: Layout,
    hidden:true,
    redirect: '/message/list',
    name: 'Message',
    meta: { title: '互动管理', icon: 'guide' },
    roles: ['attestation', 'artist','employee'],
    children: [
      {
        path: 'list',
        name: 'BrandList',
        component: () => import('@/views/message/list/index'),
        roles: ['attestation', 'artist','employee'],
        meta: { title: '关注管理',
        icon: 'guide' }
      },
      {
        path: 'fanslist',
        name: 'FansList',
        component: () => import('@/views/message/fanslist/index'),
        roles: ['attestation', 'artist','employee'],
        meta: { title: '粉丝管理',
        icon: 'guide' }
      },
      {
        path: 'create',
        name: 'MessageCreate',
        component: () => import('@/views/message/form/index'),
        hidden:true,
        meta: { title: '添加消息',
        roles: ['attestation', ],}
      },
      {
        path: 'update:id',
        name: 'MessageUpdate',
        hidden:true,
        component: () => import('@/views/message/form/edit'),
        hidden:true,
        meta: { title: '修改消息',
        roles: ['attestation', ], }
      }
    ]
  },
  //购物车管理
  {
    path: '/cart',
    component: Layout,
    redirect: '/cart/list',
    hidden:true,
     // will always show the root menu
    name: '购物车管理',
    meta: {
      title: '购物车管理',
      icon: 'pc-manifest-w',
      roles: ['attestation','artist'],
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/cart/list'),
        name: '购物车管理',
        meta: {
          title: '购物车管理',
          roles: ['attestation','artist'],
          icon: 'pc-manifest-w',
          // or you can only set roles in sub nav
        }
      },
      {
        path: 'form',
        component: () => import('@/views/cart/form'),
        hidden:true,
        name: '添加购物车',
        meta: {
          title: '添加购物车',
          roles: ['attestation','artist'],
        },
      },
      {
        path: 'edit:id',
        hidden: true,
        name: 'cartModify',
        component: () => import('@/views/cart/modify'),
        hidden:true,
        meta: { title: '编辑购物车',
        roles: ['attestation','artist'],}
      }
    ]
  },
  //操作记录管理
  {
    path: '/operation',
    component: Layout,
    redirect: '/operation/list',
    hidden: true,
     // will always show the root menu
    name: 'operation',
    meta: {
      title: '操作记录管理',
      icon: 'pc-manifest-w',
      roles: ['attestation','artist'],
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/operation/list'),
        hidden: true,
       name:'operationList',
        meta: {
          title: '操作记录管理',
          icon: 'pc-manifest-w',
          roles: ['attestation','artist'],
          // or you can only set roles in sub nav
        }
      },
      {
        path: 'form',
        component: () => import('@/views/operation/form'),
        hidden:true,
        name: 'addOperation',
        meta: {
          title: '修改订单状态',
          roles: ['attestation','artist'],
        },
      },
      {
        path: 'edit:id',
        hidden: true,
        name: 'operationModify',
        component: () => import('@/views/operation/modify'),
        hidden:true,
        meta: { title: '编辑操作记录',
        roles: ['attestation','artist'],}
      }
    ]
  },
  //管理员管理
  {
    path: '/manager',
    component: Layout,
    hidden: true,
    redirect: '/manager/list',
     // will always show the root menu
    
    meta: {
      title: '管理员管理',
      icon: 'lock',
      roles: ['attestation'],
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/manager/list'),
        name: '管理员管理',
        meta: {
          title: '管理员管理',
          roles: ['attestation'],
          // or you can only set roles in sub nav
        }
      },
      {
        path: 'form',
        component: () => import('@/views/manager/form'),
        hidden:true,
        name: 'artistCreate',
        meta: {
          title: '创建管理员',
          roles: ['attestation'],
        },
      },
      {
        path: 'edit:id',
        hidden: true,
        name: 'MannagerModify',
        component: () => import('@/views/manager/modify'),
        hidden:true,
        meta: { title: '修改管理员',
        roles: ['attestation'],}
      }
    ]
  },
  //兑换
  {
    path: '/change',
    component: Layout,
    redirect: '/change/search',
    name: 'Change',
    hidden:true,
    meta: { title: '兑换管理', icon: 'example',roles: ['attestation','artist'], },
    children: [
      {
        path: 'search',
        name: 'ChangeList',
        component: () => import('@/views/change/search/index'),
        meta: { title: '兑换列表', icon: 'table',roles: ['attestation','artist'], }
      },
      {
        path: 'modify',
        name: 'ChangeModify',
        hidden:true,
        component: () => import('@/views/change/modify/index'),
        meta: { title: '编辑兑换', icon: 'form',roles: ['attestation','artist'], },
     
      }
      // {
      //   path: 'export',
      //   name: 'ChangeExport',
      //   component: () => import('@/views/order/export/index'),
      //   meta: { title: '订单导出', icon: 'table' }
      // }
    ]
  },
  //报表
  {
    path: '/report',
    component: Layout,
    redirect: '/report/recharge',
    name: 'Report',
    hidden:true,
    meta: { title: '报表', icon: 'chart' , roles: ['attestation','artist'],},
    children: [
      {
        path: 'recharge',
        name: 'ReportRecharge',
        component: () => import('@/views/report/recharge'),
        meta: { title: '充值记录', icon: 'table' , roles: ['attestation','artist'],}
      },
      {
        path: 'goods',
        name: 'ReportGoods',
        component: () => import('@/views/report/goods'),
        meta: { title: '销售记录', icon: 'table', roles: ['attestation','artist'], }
      }
    ]
  },
  // {
  //   path: '/dashboard',
  //   component: Layout,
  //   redirect: '/dashboard',
  //   children: [
  //     {
  //       path: 'dashboard',
  //       component: () => import('@/views/dashboard/index'),
  //       name: '图表',
  //       meta: { title: '图表', icon: 'dashboard', affix: false }
  //     }
  //   ]
  // },
  // {
  //   path: '/documentation',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/documentation/index'),
  //       name: 'Documentation',
  //       meta: { title: 'Documentation', icon: 'documentation', affix: false }
  //     }
  //   ]
  // },
  // {
  //   path: '/guide',
  //   component: Layout,
  //   redirect: '/guide/index',
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/guide/index'),
  //       name: 'Guide',
  //       meta: { title: 'Guide', icon: 'guide', noCache: true }
  //     }
  //   ]
  // },
  // {
  //   path: '/profile',
  //   component: Layout,
  //   redirect: '/profile/index',
  //   hidden: true,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/profile/index'),
  //       name: 'Profile',
  //       meta: { title: 'Profile', icon: 'user', noCache: true }
  //     }
  //   ]
  // },
  
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [

  

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),

  routes: constantRoutes
})

const router = createRouter()
router.beforeEach((to, from, next) => {
  if (to.matched.length ===0) {  //如果未匹配到路由
    from.path? next({ path:from.path}) : next('/');   //如果上级也未匹配到路由则跳转主页面，如果上级能匹配到则转上级路由
  } else {
    next();    //如果匹配到正确跳转
  }
});
// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
