import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const RoleKey='Role';
const rolearr=['admin','manager','employee'];
export function getToken() {
  console.log("重新获取token");
  // console.log(Cookies.get(TokenKey))
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
export function getRole() {
  return Cookies.get(RoleKey)
}
export function setRole(role) {
  return Cookies.set(RoleKey, role)
}
