import defaultSettings from '@/settings'

const title = defaultSettings.title || '象幕MCN后台管理系统管理系统'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
