import Cookies from 'js-cookie'
export function getStoreId() {
    return JSON.parse(Cookies.get('userinfo')).storeId
}
export function getRoles() {
    return JSON.parse(Cookies.get('userinfo')).roles[0]
}