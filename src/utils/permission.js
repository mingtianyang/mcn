// import store from '@/store'
import Cookies from 'js-cookie'
/**
 * @param {Array} value
 * @returns {Boolean}
 * @example see @/views/permission/directive.vue
 */
export default function checkPermission(value) {
//  console.log("判断是否有权限编辑或修改")
//   console.log(value)
  if (value && value instanceof Array && value.length > 0) {
    const roles =JSON.parse(Cookies.get('Role'));
    // console.log(roles);
    const permissionRoles = value
    const hasPermission = roles.some(role => {
      return permissionRoles.includes(role)
    })

    if (!hasPermission) {
      // console.log(false);
      return false
    }
    // console.log(true);
    return true
  } else {
    // console.error(`need roles! Like v-permission="['admin','editor']"`)
    return false
  }
}
/**
 * @param {Array} value
 * @returns {Boolean}
 * @example see @/views/permission/directive.vue
 */



